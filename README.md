# HACED

HACED est un jeu de tir TPS où le principe est de sortir vivant d'un hangar infecté de zombies.  
Il faudra trouver une zone d'extraction. Pour cela, vous aurez 5 minutes.
Pour vous aider, vous trouverez des kits de soins et de munitions dans la map.
Si vous réussissez, vous intègrerez les rangs du S.W.A.T.

Bonne chance !
